var gulp = require('gulp'),
  //html2pug = require('gulp-html2pug'),
  pug = require('gulp-pug'),
  clean = require('gulp-clean'),
  watch = require('gulp-watch'),
  copy = require('gulp-copy');

gulp.task('clean:dist', function () {
  return gulp.src('./dist')
    .pipe(clean({read: false}))
});

gulp.task('pug2html', function () {
  gulp.src('./*.pug')
    .pipe(pug())
    .pipe(gulp.dest('./dist'))
});

gulp.task('copy:css', function () {
  gulp.src([
      './bower_components/bootstrap/dist/css/bootstrap.min.css',
      './bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
      './css/modern-business.css'
    ])
    .pipe(gulp.dest('./dist/css'))
});

gulp.task('copy:js', function () {
  gulp.src([
      './bower_components/bootstrap/dist/js/bootstrap.min.js',
      './bower_components/jquery/dist/jquery.min.js'
    ])
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('copy:fonts', function () {
  gulp.src([
      './bower_components/bootstrap/dist/fonts/*.{eot,svg,woff,woff2,ttf}'
    ])
    .pipe(gulp.dest('./dist/fonts'))
});

gulp.task('copy:font-awesome', function () {
  gulp.src('./font-awesome/**/*.{otf,eot,svg,ttf,woff,css}')
    .pipe(gulp.dest('./dist/font-awesome'))
});

gulp.task('clean', ['clean:dist']);
gulp.task('copy', ['copy:css', 'copy:js', 'copy:fonts', 'copy:font-awesome']);
gulp.task('default', ['copy', 'pug2html', 'watch']);

gulp.task('watch', function () {
  gulp.watch(['./*.pug'], function (e) {
    gulp.run('pug2html')
  })
});